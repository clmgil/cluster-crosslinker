#!/usr/bin/env python3
'''
Compare 2 protein FASTA files.

Perform pairwise alignment using MUSCLE between all sequences in
smallest file against all sequences in the other file.

Calculates percent-identity, then finds best hits by identifying
statistical outliers in all hits for that query.

e.g. 1 query, 20 pairwise alignments --> 19/20 ~10% identities, 1 has ~60%
    60% is 2sd's > mean --> print

Author: Cameron Gilchrist
Date: 2018-07-17
'''

from Bio import SeqIO
from Bio import AlignIO
from Bio.Align.Applications import MuscleCommandline
import io
import sys
import argparse
import numpy as np

def read_sequences(fasta):
    '''Return list of SeqRecords from SeqIO.parse FASTA object'''
    sequences = []
    for seq in fasta:
        sequences.append(seq)
    return(sequences)

def med_abs_dev(array):
    '''Calculate Median Absolute Deviation (MAD) for finding outliers.

    More robust than using 2*std dev + mean'''

    median = np.median(array)
    diff = np.abs(array - median)
    mad = np.median(diff)
    return(mad, median)

def run_muscle(first, second, args):
    '''Run MUSCLE aligner via command line call.
    '''
    def _calculate_identity(align):
        '''Calculate percent identity of given alignment.

        Logic taken from:
        http://www.bioinformatics.org/sms2/ident_sim.html
        '''
        # Grab aligned sequences
        a = list(align[0])
        b = list(align[1])
        # Groups of similar AA's for similarity calculation
        similar_aa = [
                ['G', 'A', 'V', 'L', 'I'],
                ['F', 'Y', 'W'],
                ['C', 'M'],
                ['S', 'T'],
                ['K', 'R', 'H'],
                ['D', 'E', 'N', 'Q'],
                ['P']
                ]
        # Counters
        matches = 0
        similar = 0
        aln_length = len(a)
        # Iterate length of full alignment inc. gaps
        for i in range(len(a)):
            if a[i] == b[i]:
                # Ensure not a gap...
                if a[i] != '-' and a[i] != '.':
                    matches += 1
                # ...otherwise -1 from alignment length
                else:
                    aln_length -= 1
            else:
                # Iterate over groups of similar AA's; tally if both in group
                for j in similar_aa:
                    if a[i] in j and b[i] in j:
                        similar += 1

        # Calculate percent-identity and similarity; return
        pident = (matches/aln_length) * 100
        similarity = ((matches + similar) / aln_length) * 100
        return(pident, similarity)

    def find_outlier(aligns):
        '''Find 'outlier' in list of alignments (i.e. highest pident)

        List of lists, formatted [seq1_name, seq2_name, pident, similarity, aln]'''
        # Make numpy array of alignment pidents to find mean/standard deviation
        id_array = np.array([x[2] for x in aligns])
        id_mean = np.mean(id_array, axis = 0)
        id_sd = np.std(id_array, axis = 0)
        id_mad, id_med = med_abs_dev(id_array)
        # Do the same for similarity
        sim_array = np.array([x[3] for x in aligns])
        sim_mean = np.mean(sim_array, axis = 0)
        sim_sd = np.std(sim_array, axis = 0)
        sim_mad, sim_med = med_abs_dev(sim_array)

        outlier = [x for x in aligns if (
            (0.6745 * (x[2] - id_med) / id_mad > 3.5) and
            (0.6745 * (x[3] - sim_med) / sim_mad > 3.5))]

        return(outlier)

    # Format MUSCLE command; clustalw out format
    muscle = MuscleCommandline(
            clwstrict=True, diags = True,
            gapopen = -10.0, gapextend = -0.5, center = 0.0,
            matrix = './BLOSUM62.txt',
            maxiters = 8
            )

    # Iterate over sequences in smallest file
    for seq1 in first:
        alignments = []
        for seq2 in second:
            # Create handle to hold faux FASTA file
            handle = io.StringIO()
            # Write sequences to handle
            SeqIO.write((seq1, seq2), handle, 'fasta')
            data = handle.getvalue()
            # Run MUSCLE
            stdout, stderr = muscle(stdin = data)
            # Read alignment
            align = AlignIO.read(io.StringIO(stdout), 'clustal')
            # Print identities
            pident, similarity = _calculate_identity(align)
            alignments.append([seq1.id, seq2.id, pident, similarity, align])
            # Print every alignment if user specifies
            if args.showall:
                print('{},{},{:.2f},{:.2f}'.format(
                    seq1.id, seq2.id,
                    pident, similarity))

        # Find best hit/s of the bunch by checking if it's a statistical outlier
        # i.e. outside 2sd's of the mean of pident's
        best = find_outlier(alignments)
        # Label as best hit if showing all alignments
        for b in best:
            if args.showall:
                best_str = 'BEST: {},{},{:.2f},{:.2f}'.format(b[0], b[1], b[2], b[3])
            else:
                best_str = '{},{},{:.2f},{:.2f}'.format(b[0], b[1], b[2], b[3])
            print(best_str)

            # Print alignments to file in clustal format
            if args.file:
                best_alns = [b[4] for b in best]
                with open(args.file, 'a') as o:
                    count = AlignIO.write(best_alns, o, 'clustal')

def main(args):
    # Read in fasta files
    one = SeqIO.parse(args.one, 'fasta')
    two = SeqIO.parse(args.two, 'fasta')
    # Parse to obtain SeqRecords
    one_seqs = read_sequences(one)
    two_seqs = read_sequences(two)
    # Determine largest file; use smallest as queries
    if len(one_seqs) >= len(two_seqs):
        run_muscle(two_seqs, one_seqs, args)
    else:
        run_muscle(one_seqs, two_seqs, args)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
            description = '''
            Find homology between proteins in two FASTA files.

            Finds smallest file, then iterates over proteins in that file performing
            pairwise alignments using MUSCLE against all proteins in the larger file.

            Prints best alignments to terminal with identity and similarity percentages.

            Usage: python3 globaligner.py <file1> <file2> <optional flags>

            Author: Cameron Gilchrist
            Date: 2018-07-20''',
            formatter_class = argparse.RawTextHelpFormatter)
    parser.add_argument('one', help = 'First protein FASTA file')
    parser.add_argument('two', help = 'Second protein FASTA file')
    parser.add_argument('--showall', help = 'Show all alignments', action = 'store_true')
    parser.add_argument('--file', help = 'File to hold aligned sequences')
    args = parser.parse_args()
    main(args)

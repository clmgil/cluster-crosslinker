#!/usr/env/bin python3
'''
Script to create publication quality gene cluster comparison figures
a la https://doi.org/10.1371/journal.ppat.1006670

Heavily adapted from https://github.com/petercim/Arrower

Author: Cameron Gilchrist
Date: 2018-07-13
'''

import sys
import argparse
from Bio import SeqIO
from collections import defaultdict
from itertools import tee

class Cluster:
    '''Object to hold taxanomic & biosynthetic information about a BGC'''
    def __init__(self, code, organism, cluster_num, scaffold_loc):
        self.code = code
        self.organism = organism
        self.cluster_num = cluster_num
        self.scaffold_loc = scaffold_loc
        self.genes = defaultdict(list)
        self.length = 0
        self.synthase_x = 0

    def update_genes(self, scaling):
        """Parse GenBank file of this cluster given it's code"""
        # Read in GenBank file matching code given in .tsv
        genbank = SeqIO.read('{}.gbk'.format(self.code), 'genbank')
        # Iterate over features
        for f in genbank.features:
            if f.type == 'CDS':#'gene':
                try:
                    locus = f.qualifiers['locus_tag'][0]
                except:
                    locus = f.qualifiers['gene'][0]
                g = self.genes[locus]
                # Populate attributes of pre-existing Gene objects
                g.start = int(f.location.start) / scaling
                g.end = int(f.location.end) / scaling
                if f.strand == '+' or f.strand == 1:
                    g.strand = 1
                elif f.strand == '-' or f.strand == -1:
                    g.strand = -1

                if g.is_synthase:
                    self.synthase_x = g.start

        # Length for svg drawing purposes
        self.length = len(genbank.seq)

class Gene:
    '''A gene feature from given GenBank file'''
    def __init__(self, locus):
        self.locus = locus
        self.start = None
        self.end = None
        self.strand = None
        self.is_synthase = False
        self.in_bgc = False
        self.arrow_top = None
        self.arrow_bot = None

def parse_visual_info(tsv):
    '''Parse TSV containing species and gene information'''
    clusters = []
    with open(tsv, 'rU') as f:
        for line in f:
            # Get species information, create new Cluster object
            if line.startswith('#'):
                code, organism, cnum, loc = line.replace('#','').strip().split(',')
                c = Cluster(code, organism, cnum, loc)
                clusters.append(c)
                continue

            # Add Gene objects to Cluster object genes attribute
            locus, synthase, cluster = line.strip().split(',')
            g = Gene(locus)
            # Set flags for colouring later
            if synthase == 'Y':
                g.is_synthase = True
            if cluster == 'Y':
                g.in_bgc = True
            c.genes[locus] = g

    return(clusters)

class Drawing(object):
    '''Functions for drawing parts of image'''
    def __init__(self, width, height):
        self.canvas = '''
<svg width="{}" height="{}"
    version="1.1"
    baseProfile="full"
    xlmns="http://www.w3.org/2000/svg"
    xlmns:xlink="http://www.w3.org/1999/xlink"
    xlmns:ev="http://www.w3.org/2001/xml-events">
    '''.format(width, height)

    def add_cluster(self, cluster):
        self.canvas += cluster

    def draw_crosslinks(self, pairs, head_locs):
        '''Iterate over dictionary of pairs, drawing links between genes.

        Draw polygon using left/right points of each gene.
        No border; opacity = pident (%)

        i.e.
                A--------------B
               /              /
              /              /
             D--------------C
        '''

        for i in sorted(pairs):
            # Define corners of polygon
            A = head_locs[i[0]][0]
            B = head_locs[i[0]][1]
            C = head_locs[i[1]][1]
            D = head_locs[i[1]][0]

            # Create a shade of grey based on identity
#            colour = '#{:02x}{:02x}{:02x}'.format(
#                    int(255*(float(i[2])/100)),
#                    int(255*(float(i[2])/100)),
#                    int(255*(float(i[2])/100))
#                    )

            # Create polygon
            link = '''
            <polygon id="link" points="{},{},{},{},{},{},{},{}"
            fill="#000000" fill-opacity="{}">
            </polygon>
            '''.format(
                    A[0], A[1],
                    B[0], B[1],
                    C[0], C[1],
                    D[0], D[1],
                    float(i[2])/100)

            self.canvas += link

    def end_svg(self):
        self.canvas += '''
        </svg>
        '''

    def save(self, filename):
        with open(filename, 'w') as out:
            out.write(self.canvas)

class Cluster_SVG:
    '''SVG representation of a cluster'''
    def __init__(self):
        self.block = ''

    def create_base_line(self, X, Y, L):
        '''Create guide line underneath cluster'''
        base_line = '''
        <line x1="{}" y1="{}" x2="{}" y2="{}"
        stroke="rgb(99,99,99)" stroke-width="2"/>
        '''.format(X, Y, X + L, Y)

        self.block += base_line

    def create_gene_arrow(self, X, Y, L, H, strand, h, l, colour, locus, head_locs):
        '''SVG code for gene arrow, taken from petercim/Arrower:
            - (X,Y) ... upper left (+) or right (-) corner of the arrow
            - L ... arrow length
            - H ... arrow height
            - strand
            - h ... arrow head edge width
            - l ... arrow head length
            - color

                    C
                    | \
        A-----------B  \
        |               D
        G-----------F  /
                    | /
                    E
        '''
        if strand == 1 or strand == None:

            # (x, y) coordinates for each point of arrow
            A = [X, Y]
            B = [X+L-l, Y]
            C = [X+L-l, Y-h]
            D = [X+L, Y + H/2]
            E = [X+L-l, Y+H+h]
            F = [X+L-l, Y+H]
            G = [X, Y+H]

            if L < l:
                # Squeeze arrow if length is shorter than head length
                B = [X, Y]
                C = [X, Y-h]
                D = [X+L, Y+H/2]
                E = [X, Y+H+h]
                F = [X, Y+H]

            left = [(A[0]+G[0])/2, (A[1]+G[1])/2]
            right = D

        elif strand == -1:

            A = [X+L, Y]
            B = [X+l, Y]
            C = [X+l, Y-h]
            D = [X, Y+H/2]
            E = [X+l, Y+H+h]
            F = [X+l, Y+H]
            G = [X+L, Y+H]

            if L < l:
                # Squeeze arrow if length shorter than head length
                B = [X+L, Y]
                C = [X+L, Y-h]
                D = [X, Y+H/2]
                E = [X+L, Y+H+h]
                F = [X+L, Y+H]

            left = D
            right = [(A[0]+G[0])/2, (A[1]+G[1])/2]

        # Create arrow polygon using defined points
        arrow = '''
        <polygon id="{}" points="{},{},{},{},{},{},{},{},{},{},{},{},{},{}"
        fill="#{}" fill-opacity="1.0" stroke="#000000" stroke-width="1">
        </polygon>
        '''.format(
                locus,
                A[0], A[1],
                B[0], B[1],
                C[0], C[1],
                D[0], D[1],
                E[0], E[1],
                F[0], F[1],
                G[0], G[1],
                colour)

        # Get coordinates for crosslinks (midpoints of A+G, B+F)
        #head_locs[locus] = [C, E] #for top/bottom points of arrow head
        head_locs[locus] = [left, right]
        # Add to cluster block
        self.block += arrow

    def write_gene_name(self, text, X, Y, F):
        name = '''
        <text x="{}" y="{}"
        font-family="Arial"
        font-size="{}"
        font-style="italic">{}</text>
        '''.format(X, Y, F, text) #text.split('_')[1])

        self.block += name

    def write_cluster_name(self, X, Y, cluster):
        '''Write species information to left of baseline.
        cluster = Cluster object'''

        species = '''
        <text x="{}" y="{}"
        text-anchor="end"
        font-family="Arial"
        font-size="12"
        font-weight="bold"
        font-style="italic">{}</text>
        '''.format(X, Y, cluster.organism)

        cluster_pos = '''
        <text x="{}" y="{}"
        text-anchor="end"
        font-family="Arial"
        font-size="12"
        font-weight="bold">{}</text>
        '''.format(X, Y+14, cluster.scaffold_loc)

        cluster_num = '''
        <text x="{}" y="{}"
        text-anchor="end"
        font-family="Arial"
        font-size="12"
        font-weight="bold">Cluster {}</text>
        '''.format(X, Y+28, cluster.cluster_num)

        self.block += species
        self.block += cluster_num
        self.block += cluster_pos

def pairwise(iterable):
    '''Itertools recipe to group list elements into pairs.
    e.g. [s0, s1, s2, s3] -> (s0,s1), (s1,s2), (s2, s3), ...'''
    a, b = tee(iterable)
    next(b, None)
    return(zip(a, b))

def parse_crosslinks(csv):
    '''Read in csv for linked genes, return list of tuples

    Edited to take one pair with pident/sim
    Then edit to take multiple'''

    #pairs = defaultdict(list)
    pairs = []
    with open(csv, 'rU') as f:
        for line in f:
            pairs.append(line.strip().split(','))

            # For dynamic number of lists
#            i = 0
#            for p in pairwise(tmp):
#                pairs[i].append(p)
#                i += 1
    return(pairs)

def main(args):
    # Parameters to control arrow shape and size
    arrow_width = 10#12
    arrow_head_width = 0#3#5
    arrow_head_length = 6
    side_margin = 180
    top_margin = 30
    scale_factor = 120.0 #70.0
    font_size = 8

    # Read in cluster information tsv, create Cluster objects
    clusters = parse_visual_info(args.info)

    # Iterate over clusters once to get necessary alignment information
    max_cluster_length = 0
    max_cluster = None
    for i in range(len(clusters)):
        # Get gene positional information from corresponding
        # GenBank files, named with same 'code' given in tsv
        cluster = clusters[i]
        cluster.update_genes(scale_factor)
        # Find length of biggest cluster
        if cluster.length > max_cluster_length:
            max_cluster_length = cluster.length
            max_cluster = cluster

    # Iterate again to draw features
    cluster_svgs = []
    head_locs = {} # hold tops and bottoms of gene arrow heads for crosslinks
    for i in range(len(clusters)):
        # Add to top margin if not the first cluster to draw below
        if i != 0:
            top_margin += 30
        # Create new Cluster SVG object
        c = Cluster_SVG()
        # Add track line
        c.create_base_line(
                side_margin,
                top_margin + arrow_width/2,
                max_cluster_length/scale_factor
                )
        # Label track with cluster/species information
        c.write_cluster_name(
                side_margin - len(cluster.organism) - 10,
                top_margin,
                clusters[i])

        # If this cluster isn't the biggest, get the difference between
        # the xpos of the large cluster synthase and current cluster synthase
        # Adds offset to elements in smaller cluster to align it with bigger one
        if clusters[i] != max_cluster:
            offset = max_cluster.synthase_x - clusters[i].synthase_x
            for gene in max_cluster.genes:
                g = max_cluster.genes[gene]
                #if g.is_synthase:
                    #offset += (g.end - g.start)/4
        else:
            offset = 0

        for gene in clusters[i].genes:
            # Set colours of genes in genbank file
            # Green if in-BGC, blue if synthase backbone,
            # grey if neither
            g = clusters[i].genes[gene]
            colour = 'cccccc' # By default
            if g.in_bgc:
                colour = '2eaa54'
            if g.is_synthase:
                colour = '1fade4'

            # Create arrow for current gene
            c.create_gene_arrow(
                    g.start + side_margin + offset, # X ... xpos of upper left (+)/right (-)
                    top_margin,                     # Y ... ypos of ""
                    g.end - g.start,                # L ... arrow length
                    arrow_width,                    # H ... arrow height
                    g.strand,                       # strand
                    arrow_head_width,               # h ... arrow head edge width
                    arrow_head_length,              # l ... arrow head length
                    colour,                         # colour
                    g.locus,                        # locus
                    head_locs                       # head_locs ... dict to hold arrow points
                    )

            # Write locus_tag above gene arrow
            c.write_gene_name(
                    g.locus,
                    side_margin + offset + g.start + (g.end - g.start)/2 - len(g.locus)*font_size/6,
                    top_margin - 10,
                    font_size
                    )

        cluster_svgs.append(c)


    # Assemble and save SVG
    drawing = Drawing((max_cluster_length)/scale_factor + 220, top_margin+40)
    # Draw crosslinks
    if args.links:
        pairs = parse_crosslinks(args.links)
        drawing.draw_crosslinks(pairs, head_locs)
    # Add gene clusters
    for c in cluster_svgs:
        drawing.add_cluster(c.block)
    drawing.end_svg()
    drawing.save(args.out)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
            description = '''
            Create publication quality SVG image of biosynthetic gene cluster homology.

            Heavily adapted from https://github.com/petercim/Arrower

            Requires genbank files for each cluster as well as information files:

            1. CSV file with cluster gene information formatted like:
                #<bgc code>,<strain info>,<cluster number>,<location on scaffold>
                <GENE1><is synthase Y/N><is in bgc Y/N>

                #ahan_c48,A. hancockii FRR 3425,48,46:18314-66465
                AHAN_008565,N,N ... this locus not a synthase, not in bgc
                AHAN_008566,Y,Y ... this locus is a synthase, is in bgc

                The synthase part is mainly for alignment purposes (e.g. line up BGCs
                based on position of a PKS or NRPS)

            2. CSV file with linking information (generated by galign_proteins.py)
                <bgc1>,<bgc2>,<id%>,<sim%>

                gene1,gene2,50.00,60.00
                gene2,gene3,60.00,70.00

                A file like this is generated by globaligner.py

            GenBank files must be named same as BGC code in #1 and in same dir
            e.g. code = ahan_c48, genbank = ahan_c48.gbk

            Author: Cameron Gilchrist
            Date: 2018-07-13''', formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('info', help = 'Information about genes in cluster')
    parser.add_argument('--links', help = 'CSV with genes to be linked')
    parser.add_argument('out', help = 'Filename for outputted SVG image')
    args = parser.parse_args()

    main(args)

#!/usr/env/bin python3
'''
Extract amino acid sequence for all CDS in a given GenBank file.

Usage: python extractgenes.py [.gbk]
Author: Cameron Gilchrist
Date: 2017-11-06
'''

from Bio import SeqIO
import sys, textwrap
import argparse

class Gene:
    '''Object to hold all sequence features'''
    def __init__(self, name):
        self.name = name
        self.nt_start = None
        self.nt_end = None
        self.nt_seq = ''
        self.nt_len = None
        self.aa_seq = ''
        self.aa_len = None

    def calc_lengths(self):
        '''Calculate lengths of nucleotide and amino acid sequences'''
        self.aa_len = len(self.aa_seq)
        self.nt_len = self.nt_end - self.nt_start

def build_gene(feat, scaffold, seqtype):
    '''Create gene object for current CDS feature and populate attributes.

    Takes SeqFeature Object, current scaffold (for nt sequence) and user
    input [nt/aa/both].
    '''
    # Grab gene/protein name
    try:
        name = feat.qualifiers['locus_tag'][0]
    except:
        try:
            name = feat.qualifiers['gene'][0]
        except(e):
            raise(e)

    # Populate Gene object
    gene = Gene(name)
    gene.nt_start = min(feat.location)
    gene.nt_end = max(feat.location)
    gene.nt_seq = scaffold[gene.nt_start:gene.nt_end]
    gene.aa_seq = feat.qualifiers['translation'][0]
    gene.calc_lengths()

    return(gene)

def fasta_string(name, seq):
    '''Create nicely formatted, 80-char wide FASTA sequence.
    '''
    fasta = '>{}\n{}'.format(
            name,
            textwrap.fill(str(seq), width = 80)
            )
    return(fasta)

def write_fasta(genes, out, seqtype):
    '''Write FASTA sequences to files - .fna for nt, .faa for aa
    '''
    # Open files for writing FASTA
    nt_out = None
    aa_out = None
    if seqtype == 'nt' or seqtype == 'both':
        nt_out = open('{}.fna'.format(out), 'w')
    if seqtype == 'aa' or seqtype == 'both':
        aa_out = open('{}.faa'.format(out), 'w')

    # Iterate all Gene objects, write multi-FASTA files.
    # Try/Excepts because better to try than ask
    for gene in genes:
        if nt_out:
            nt_fasta = fasta_string(gene.name, gene.nt_seq)
            nt_out.write(
                    '{}\n'.format(nt_fasta)
                    )

        if aa_out:
            aa_fasta = fasta_string(gene.name, gene.aa_seq)
            aa_out.write(
                    '{}\n'.format(aa_fasta)
                    )

    # Close files when finished
    if nt_out:
        nt_out.close()
    if aa_out:
        aa_out.close()

def print_length_table(genes):
    '''Print sequence lenths of all genes to stdout.
    '''
    # Create 'table' of headers and genes/seq. lengths
    headers = [('Locus', 'Nucleotide', 'Amino Acid')]
    gene_lens = [(x.name, x.nt_len, x.aa_len) for x in genes]
    table = [*headers, *gene_lens]
    # Determine how wide each column should be given max string length
    col_width = [max(len(str(x)) for x in col) for col in zip(*table)]
    # Print table
    for line in table:
        print('  '.join(
            '{:{}}'.format(
                x, col_width[i]) for i, x in enumerate(line)
            ))

def main(args):
    '''Run program'''
    genes = []
    lengths = {}
    genbank = SeqIO.parse(args.genbank, 'genbank')
    print('Parsing {}'.format(args.genbank))
    # Find all CDS genes in all scaffolds of given GenBank; create
    # Gene objects to hold sequence information
    for record in genbank:
        # Grab sequence of current record being parsed (i.e. scaffold),
        # necessary for grabbing nucleotide sequences
        scaffold = record.seq
        for feat in record.features:
            if feat.type == 'CDS':
                gene = build_gene(feat, scaffold, args.seqtype)
                genes.append(gene)

    # Write sequences to files as multi-FASTA
    write_fasta(genes, args.out, args.seqtype)
    # Print genes & their nt/aa sequence lengths to stdout
    print_length_table(genes)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
            description = '''
            Extract all amino acid or nucleotide sequences of CDS features in a given GenBank file.

            Writes all sequences in multi-FASTA format to specified filename, and prints
            a table to stdout with both nucleotide and amino acid sequence lengths.

            Author: Cameron Gilchrist
            Date: 2018-08-09''', formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('genbank', help = 'GenBank file to extract sequences from')
    parser.add_argument('seqtype', help = 'Extract nucleotides [nt], amino acids [aa] or [both]',
            choices = ['nt', 'aa', 'both'])
    parser.add_argument('out',
            help = 'Filename prefix to write sequences to (will append .fna or .faa for nt and aa, respectively)')
    args = parser.parse_args()

    main(args)
